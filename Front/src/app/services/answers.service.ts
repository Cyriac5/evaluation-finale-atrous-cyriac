import { Injectable } from '@angular/core';
import axios from 'axios';
import { Answer } from '../models/answers-model';
import { Question } from '../models/questions-model';

@Injectable({
  providedIn: 'root'
})
export class AnswersService {

  public apiUrl: string = 'http://localhost:8080/questions';
  public answers: Answer[] = [];
  public answer?: Answer;
  public question?: Question;

  constructor() { }

  public createAnswer( acontent: String, tid: string): Promise<Answer>{
    return axios.post(this.apiUrl + '/' + tid,
      { 
        acontent: acontent
      }
     )
  }

  public getAnswersByQuestionID(tid: string) {
    axios.get(this.apiUrl + '/'+ tid + '/answers')
    .then((res) => {
    this.answers = res.data;
    console.log(this.answers)
  })
  .catch((err => {
    console.log(err);
  }))
  }
}
