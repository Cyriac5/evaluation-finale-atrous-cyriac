import { Injectable } from '@angular/core';
import { User } from '../models/users-model';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private readonly SESSION_STORAGE_KEY = "currentUser";

  login(user: User){
    sessionStorage.setItem(this.SESSION_STORAGE_KEY, JSON.stringify(user));
    
  }

  logout(){
    sessionStorage.removeItem(this.SESSION_STORAGE_KEY);
  }

  getter(): string{
    const currentUserPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY)
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain)  
      return currentUser.username;
    } else{
      return "";
    }

  }

  isLoggedIn(): boolean {
    if (sessionStorage.getItem(this.SESSION_STORAGE_KEY)) {
      return true;
    } else {
      return false;
    }
  }

  constructor() { }

  getCurrentUserBasicAuthentication(): string  {
  
    const currentUserPlain = sessionStorage.getItem(this.SESSION_STORAGE_KEY)
    if (currentUserPlain) {
      const currentUser = JSON.parse(currentUserPlain)
      return "Basic " + btoa(currentUser.username + ":" + currentUser.password);
    } else {
      return "";
    }
  }
}
