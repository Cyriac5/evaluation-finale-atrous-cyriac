import { Injectable } from '@angular/core';
import axios from 'axios';
import { Question } from '../models/questions-model';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  public apiUrl: string = 'http://localhost:8080/questions';
  public questions: Question[] = [];
  public question?: Question;


  constructor() { }

  // public async createQuestion( question: Question){
  //   console.log(question);
  //   await axios.post(this.apiUrl, question);
  // }

  public  createQuestion( title:String, qcontent: String): Promise<Question>{
    return axios.post(this.apiUrl,
      { 
        title: title,
        qcontent: qcontent
      }
     )
  }

  public fetchQuestions() {
    axios.get(this.apiUrl)
    .then((res) => {
    this.questions = res.data;
    })
    .catch((err) => {
    console.log(err);
    });
  }

  public getQuestionDetail(tid: string) {
    axios.get(this.apiUrl + '/'+ tid)
    .then((res) => {
    this.question = res.data;
  })
  .catch((err => {
    console.log(err);
  }))
  }


}
