import { Injectable } from '@angular/core';
import axios from 'axios';
import { User } from '../models/users-model';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  public apiUrl: string = 'http://localhost:8080/users';
  public user?: User;


  constructor() { }

  public createUser( username:String, password: String): Promise<User>{
    return axios.post(this.apiUrl,
      { 
        username: username,
        password: password
      }
     )
  }
}
