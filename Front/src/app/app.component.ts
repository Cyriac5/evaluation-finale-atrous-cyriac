import { Component } from '@angular/core';
import { AuthenticationService } from './services/authentication.service';
import { authInterceptor } from './_helpers/axios-interceptor';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Front';

  constructor(private authenticationService: AuthenticationService){
    authInterceptor(this.authenticationService);
  }
}
