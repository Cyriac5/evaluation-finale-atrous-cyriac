import { User } from "./users-model";

export interface Question{
    title: string;
    qcontent: string;
    tid: String;
    date: Date;
    user: User;
}

// user: string;
// private String tid;
// private String title;
// private String qcontent;
// private LocalDateTime postdate;