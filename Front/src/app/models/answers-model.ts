import { User } from "./users-model";

export interface Answer{
    acontent: string;
    tid: String;
    answerdate: Date;
    user: User;
}