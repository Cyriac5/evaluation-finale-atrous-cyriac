import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/users-model';
import { AuthenticationService } from 'src/app/services/authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form = new FormGroup({
    "username": new FormControl("", Validators.required),
    "password": new FormControl("", Validators.required)
  })

  constructor(public authenticationService: AuthenticationService, public activatedRoute: ActivatedRoute, public router: Router) { }
  
  public getValueFormLogging(): void {
    const currentUser: User = {username: this.form.get("username")?.value, password: this.form.get("password")?.value};
    this.authenticationService.login(currentUser)
    // .then().catch()
    this.router.navigate([""]);
  }



  ngOnInit(): void {
  }

}
