import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Answer } from 'src/app/models/answers-model';
import { Question } from 'src/app/models/questions-model';
import { AnswersService } from 'src/app/services/answers.service';
import { QuestionsService } from 'src/app/services/questions.service';

@Component({
  selector: 'app-question-answers',
  templateUrl: './question-answers.component.html',
  styleUrls: ['./question-answers.component.scss']
})
export class QuestionAnswersComponent implements OnInit {

  public question?: Question;
  public tid: string = "";
  public answers?: Answer[];

  constructor(public questionService: QuestionsService, private route: ActivatedRoute, public answerService: AnswersService) { }

  ngOnInit(): void {
      this.tid = this.route.snapshot.params['tid'];
      this.questionService.getQuestionDetail(this.tid);
      this.answerService.getAnswersByQuestionID(this.tid);
  }

}
