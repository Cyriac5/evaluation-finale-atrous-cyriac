import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Question } from 'src/app/models/questions-model';
import { QuestionsService } from 'src/app/services/questions.service';


@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.scss']
})
export class CreateQuestionComponent implements OnInit {
  public newQuestion?: Question;
  // title = new FormControl("", [Validators.required]);
  // qcontent = new FormControl("", [Validators.required]);
  form = new FormGroup({
    "title": new FormControl("", Validators.required),
    "qcontent": new FormControl("", Validators.required),
  });

  constructor(public questionService: QuestionsService, private router: Router) { }

  ngOnInit(): void {
  }

  async onSubmit(){
      
    this.questionService.createQuestion(this.form.get("title")?.value, this.form.get("qcontent")?.value).then(() => {
      this.router.navigate(['/']);
      
    })
  }

}
