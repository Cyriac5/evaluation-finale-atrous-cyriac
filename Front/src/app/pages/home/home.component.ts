import { Component, OnInit } from '@angular/core';
import { Question } from 'src/app/models/questions-model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { QuestionsService } from 'src/app/services/questions.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  

  constructor(public questionService: QuestionsService, public authenticationService: AuthenticationService) { }

  ngOnInit(): void {
    this.questionService.fetchQuestions();
  }

}
