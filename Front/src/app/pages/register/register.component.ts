import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from 'src/app/models/users-model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  form = new FormGroup({
    "username": new FormControl("", Validators.required),
    "password": new FormControl("", Validators.required),

  })

  public currentUser?: User;

  constructor(public authenticationService: AuthenticationService, public activatedRoute: ActivatedRoute, public router: Router, public userService: UsersService) { }
  
  // public getValueFormLogging(): void {
  //   const currentUser: User = {username: this.form.get("username")?.value, password: this.form.get("password")?.value};
  //   this.authenticationService.login(currentUser)
  //   // .then().catch()
  //   this.router.navigate([""]);
  // }

  async onSubmit(){
      
    await this.userService.createUser(this.form.get("username")?.value, this.form.get("password")?.value)
    .then(() => {
      this.router.navigate(['']);
    })
    this.currentUser = {username: this.form.get("username")?.value, password: this.form.get("password")?.value};
    this.authenticationService.login(this.currentUser);

  }


  ngOnInit(): void {
  }

}
