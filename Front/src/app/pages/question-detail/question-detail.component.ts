import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Answer } from 'src/app/models/answers-model';
import { Question } from 'src/app/models/questions-model';
import { AnswersService } from 'src/app/services/answers.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { QuestionsService } from 'src/app/services/questions.service';

@Component({
  selector: 'app-question-detail',
  templateUrl: './question-detail.component.html',
  styleUrls: ['./question-detail.component.scss']
})
export class QuestionDetailComponent implements OnInit {

  public question?: Question;
  public tid: string = "";
  isShow = false;



  constructor(public questionService: QuestionsService, private route: ActivatedRoute, public answerService: AnswersService, public authenticationService: AuthenticationService) { }

  displayForm(){
    this.isShow = !this.isShow;
  }

  ngOnInit(): void {
      this.tid = this.route.snapshot.params['tid'];
      this.questionService.getQuestionDetail(this.tid);
      this.answerService.getAnswersByQuestionID(this.tid);
      this.displayForm();
  }

}
