import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateQuestionComponent } from './pages/create-question/create-question.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { QuestionAnswersComponent } from './pages/question-answers/question-answers.component';
import { QuestionDetailComponent } from './pages/question-detail/question-detail.component';
import { RegisterComponent } from './pages/register/register.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'questions', component: CreateQuestionComponent},
  { path: 'questions/:tid', component: QuestionDetailComponent},
  { path: 'questions/:tid/answers', component: QuestionAnswersComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
