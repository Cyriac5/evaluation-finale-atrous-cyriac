import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Answer } from '../models/answers-model';
import { AnswersService } from '../services/answers.service';
import { QuestionsService } from '../services/questions.service';


@Component({
  selector: 'app-create-answer',
  templateUrl: './create-answer.component.html',
  styleUrls: ['./create-answer.component.scss']
})
export class CreateAnswerComponent implements OnInit {

  public newAnswer?: Answer;
  public tid: string = "";



  form = new FormGroup({
    "acontent": new FormControl("", Validators.required),
  });

  constructor(public answerService: AnswersService, private router: Router, private route: ActivatedRoute, public questionService: QuestionsService) { }

  async onSubmit(){
      
    this.answerService.createAnswer(this.form.get("acontent")?.value, this.tid).then(() => {
      // this.router.navigate(['/questions']);
      window.location.reload();
      
    })
  }

  ngOnInit(): void {
    this.tid = this.route.snapshot.params['tid'];

  }

}
