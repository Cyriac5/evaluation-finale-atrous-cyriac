import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { CreateQuestionComponent } from './pages/create-question/create-question.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './pages/login/login.component';
import { QuestionDetailComponent } from './pages/question-detail/question-detail.component';
import { QuestionAnswersComponent } from './pages/question-answers/question-answers.component';
import { CreateAnswerComponent } from './create-answer/create-answer.component';
import { RegisterComponent } from './pages/register/register.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CreateQuestionComponent,
    HeaderComponent,
    LoginComponent,
    QuestionDetailComponent,
    QuestionAnswersComponent,
    CreateAnswerComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
