package com.zenika.faq.application;

import com.zenika.faq.domain.model.answers.MissingQuestionMarkException;
import com.zenika.faq.domain.model.questions.BadLengthQuestionTitleException;
import com.zenika.faq.domain.model.questions.Question;
import com.zenika.faq.domain.model.users.User;
import com.zenika.faq.domain.repository.QuestionRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
@ActiveProfiles("test")
public class QuestionServiceTest {
    @MockBean
    UserService userService;
    @MockBean
    QuestionRepository questionRepository;


    @Autowired
    QuestionService questionService;

    @Test
    @WithMockUser(username = "dummy")
    void testCreateArticleOk() throws BadLengthQuestionTitleException, MissingQuestionMarkException {
//Mock UserService
        Mockito.when(userService.getUserByUsername("dummy")).thenReturn(Optional.of(new User("456", "dummy", "dummypassword")));
//Mock QuestionRepository
        Question question = new Question("5555", "title ?", "content", LocalDateTime.now().withNano(0), null);
        Mockito.when(questionRepository.save(any(Question.class))).thenReturn(question);
//test CreateArticle
        Question createdQuestion = questionService.createQuestion(question.getTitle(), question.getQcontent());
        System.out.println(createdQuestion.getTitle());
        assertEquals(createdQuestion.getTid(), question.getTid());
    }


    @Test
    @WithMockUser(username = "dummy")
    void testCreateQuestionMarkException() {
//Mock UserService
        Mockito.when(userService.getUserByUsername(any(String.class))).thenReturn(Optional.of(new User("5555", "dummy", "dummypass")));
//Mock ArticleRepository
        Question question = new Question("1234", "test", "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Quisque nec placerat neque." + "Nulla iaculis venenatis enim, id congue massa lacinia nec." + " Donec vestibulum augue non ligula iaculis, tincidunt pellentesque nibh vehicula.Sed et posuere dui.Fusce id ipsum" + "eu arcu consequat semper. ", LocalDateTime.now().withNano(0), null);
        Mockito.when(questionRepository.save(any(Question.class))).thenReturn(question);

        assertThrows(MissingQuestionMarkException.class, () -> {
            questionService.createQuestion(question.getTitle(), question.getQcontent());
        });
    }
}