package com.zenika.faq.application;

import com.zenika.faq.domain.model.answers.MissingQuestionMarkException;
import com.zenika.faq.domain.model.questions.BadLengthQuestionTitleException;
import com.zenika.faq.domain.model.questions.Question;
import com.zenika.faq.domain.model.users.User;
import com.zenika.faq.domain.repository.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Component
public class QuestionService {

    public QuestionRepository questionRepository;
    public UserService userService;

    @Autowired
    public QuestionService(QuestionRepository questionRepository, UserService userService) {
        this.questionRepository = questionRepository;
        this.userService = userService;
    }

    public Iterable<Question> getQuestions() {
        return questionRepository.findAll(Sort.by(Sort.Direction.DESC, "postdate"));
    }

    public Optional<Question> getQuestionByTid(String tid) {
        return questionRepository.findById(tid);
    }

    public Question getQuestionByTitle(String title) {
        return questionRepository.findByTitle(title);
    }

    public Question createQuestion(String title, String qcontent) throws BadLengthQuestionTitleException, MissingQuestionMarkException {
        String[] wordsInTitle = title.split(" ");
        int numberOfWordsInTitle = wordsInTitle.length - 1;

        if (numberOfWordsInTitle > 19) {
            throw new BadLengthQuestionTitleException();
        } else if (!wordsInTitle[numberOfWordsInTitle].contains("?")) {
            throw new MissingQuestionMarkException();
        } else {
            Optional<User> userByUsername = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            User user = userByUsername.orElseThrow(RuntimeException::new);

            Question question = new Question(UUID.randomUUID().toString(), title, qcontent, LocalDateTime.now().withNano(0), user);
            return questionRepository.save(question);
        }

    }


}
