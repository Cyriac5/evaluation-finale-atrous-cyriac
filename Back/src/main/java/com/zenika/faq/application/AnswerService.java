package com.zenika.faq.application;

import com.sun.istack.Nullable;
import com.zenika.faq.domain.model.answers.Answer;
import com.zenika.faq.domain.model.answers.BadLengthAnswerContentException;
import com.zenika.faq.domain.model.questions.Question;
import com.zenika.faq.domain.model.users.User;
import com.zenika.faq.domain.repository.AnswerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class AnswerService {

    private AnswerRepository answerRepository;
    private QuestionService questionService;
    private UserService userService;

    @Autowired
    public AnswerService(AnswerRepository answerRepository, QuestionService questionService, UserService userService) {
        this.answerRepository = answerRepository;
        this.questionService = questionService;
        this.userService = userService;
    }

    public List<Answer> getAnswersByQuestionTid(String questionTid) {
        return answerRepository.findByQuestionTid(questionTid);
    }

    public Answer createAnswer(String acontent, String questionTid) throws BadLengthAnswerContentException {

        String[] wordsInContent = acontent.split(" ");
        int numberOfWordsInContent = wordsInContent.length;

        if (numberOfWordsInContent > 100) {
            throw new BadLengthAnswerContentException();
        } else {

            Optional<User> userByUsername = userService.getUserByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            User user = userByUsername.orElseThrow(RuntimeException::new);

            Answer answer = new Answer(UUID.randomUUID().toString(), acontent, LocalDateTime.now().withNano(0), questionService.getQuestionByTid(questionTid).orElse(null), user);
            answerRepository.save(answer);
            return answer;
        }


    }
}
