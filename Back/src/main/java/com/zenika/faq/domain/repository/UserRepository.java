package com.zenika.faq.domain.repository;

import com.zenika.faq.domain.model.users.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, String> {
    public Optional<User> findByUsername(String username);

}
