package com.zenika.faq.domain.model.questions;

import com.zenika.faq.domain.model.users.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "questions")
@Access(AccessType.FIELD)
public class Question {

    @Id
    private String tid;
    private String title;
    private String qcontent;
    private LocalDateTime postdate;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_tid")
    private User user;

    public Question(String tid, String title, String qcontent, LocalDateTime postdate, User user) {
        this.tid = tid;
        this.title = title;
        this.qcontent = qcontent;
        this.postdate = postdate;
        this.user = user;
    }

    protected Question() {
        //FOR JPA
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setQcontent(String qcontent) {
        this.qcontent = qcontent;
    }

    public void setDate(LocalDateTime postdate) {
        this.postdate = postdate;
    }

    public String getTid() {
        return tid;
    }

    public String getTitle() {
        return title;
    }

    public String getQcontent() {
        return qcontent;
    }

    public LocalDateTime getDate() {
        return postdate;
    }


    public User getUser() {
        return user;
    }

}
