package com.zenika.faq.domain.model.answers;

import com.zenika.faq.domain.model.questions.Question;
import com.zenika.faq.domain.model.users.User;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "answers")
@Access(AccessType.FIELD)
public class Answer {

    @Id
    private String tid;
    private String acontent;
    private LocalDateTime answerdate;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "question_tid")
    private Question question;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_tid")
    private User user;


    public Question getQuestion() {
        return question;
    }

    public Answer(String tid, String acontent, LocalDateTime answerdate, Question question, User user) {
        this.tid = tid;
        this.acontent = acontent;
        this.answerdate = answerdate;
        this.question = question;
        this.user = user;
    }

    protected Answer(){

    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public void setAcontent(String acontent) {
        this.acontent = acontent;
    }

    public void setPostdate(LocalDateTime answerdate) {
        this.answerdate = answerdate;
    }

    public String getTid() {
        return tid;
    }

    public User getUser() {
        return user;
    }

    public String getAcontent() {
        return acontent;
    }

    public LocalDateTime getAnswerdate() {
        return answerdate;
    }
}
