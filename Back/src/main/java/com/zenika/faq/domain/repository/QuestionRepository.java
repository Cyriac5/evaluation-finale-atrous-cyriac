package com.zenika.faq.domain.repository;

import com.zenika.faq.domain.model.questions.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<Question, String> {

    public Question findByTitle(String title);

}
