package com.zenika.faq.domain.repository;

import com.zenika.faq.domain.model.answers.Answer;
import com.zenika.faq.domain.model.questions.Question;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AnswerRepository extends CrudRepository<Answer,String> {
    public List<Answer> findByQuestionTid(String questionTid);
}
