package com.zenika.faq;

import com.zenika.faq.domain.model.answers.BadLengthAnswerContentException;
import com.zenika.faq.domain.model.answers.MissingQuestionMarkException;
import com.zenika.faq.domain.model.questions.BadLengthQuestionTitleException;
import com.zenika.faq.exceptions.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionsHandler {

    @ExceptionHandler(value = BadLengthQuestionTitleException.class)
    public final ResponseEntity<ApiError> badLengthQuestionTitleException(BadLengthQuestionTitleException e){
        ApiError apiError = new ApiError("Le titre ne doit pas comporter plus de 20 mots", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = BadLengthAnswerContentException.class)
    public final ResponseEntity<ApiError> badLengthAnswerContentException(BadLengthAnswerContentException e){
        ApiError apiError = new ApiError("Le contenu de la réponse ne doit pas comporter plus de 100 mots", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = MissingQuestionMarkException.class)
    public final ResponseEntity<ApiError> missingQuestionMarkException(MissingQuestionMarkException e){
        ApiError apiError = new ApiError("Le titre de la question doit se terminer par un point d'interrogation!", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(apiError, HttpStatus.BAD_REQUEST);
    }
}
