package com.zenika.faq.web.users;

import com.zenika.faq.application.UserService;
import com.zenika.faq.domain.model.users.User;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/me")
    @ResponseStatus(value = HttpStatus.OK)
    public String me() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @PostMapping()
    @ResponseStatus(value = HttpStatus.CREATED)
    public CreatedUserDto createUser(@RequestBody CreateUserRequestDto createUserDto) {
        User user = this.userService.createUser(createUserDto.getUsername(), createUserDto.getPassword());
        return new CreatedUserDto(user.getTid(), user.getUsername());
    }
}
