package com.zenika.faq.web.questions;

public record CreateQuestionRequestDto (String title, String qcontent){
}
