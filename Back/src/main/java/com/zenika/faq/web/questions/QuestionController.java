package com.zenika.faq.web.questions;


import com.zenika.faq.application.AnswerService;
import com.zenika.faq.application.QuestionService;
import com.zenika.faq.domain.model.answers.Answer;
import com.zenika.faq.domain.model.answers.BadLengthAnswerContentException;
import com.zenika.faq.domain.model.answers.MissingQuestionMarkException;
import com.zenika.faq.domain.model.questions.BadLengthQuestionTitleException;
import com.zenika.faq.domain.model.questions.Question;
import com.zenika.faq.web.answers.CreateAnswerRequestDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingMatrixVariableException;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/questions")
public class QuestionController {
    private final QuestionService questionService;
    private final AnswerService answerService;

    public QuestionController(QuestionService questionService, AnswerService answerService) {
        this.questionService = questionService;
        this.answerService = answerService;
    }

    @GetMapping
    public Iterable<Question> getAllQuestions() {
        return questionService.getQuestions();
    }

//    @GetMapping("/{questionTid}")
//    public ResponseEntity<Question> getQuestionByTid(@PathVariable String questionTid){
//        return questionService.getQuestionByTid(questionTid).map(u -> ResponseEntity.ok(u)).orElseGet(() -> ResponseEntity.notFound().build());
//    }

    @PostMapping
    public Question createQuestion(@RequestBody CreateQuestionRequestDto body) throws BadLengthQuestionTitleException, MissingQuestionMarkException {
        return questionService.createQuestion(body.title(), body.qcontent());
    }

    @PostMapping("/{questionTid}")
    public Answer createAnswer(@RequestBody CreateAnswerRequestDto body, @PathVariable String questionTid) throws BadLengthAnswerContentException {
        return answerService.createAnswer(body.acontent(), questionTid);
    }

    @GetMapping("/{questionTid}/answers")
    public List<Answer> getQuestionAnswers(@PathVariable String questionTid) {
        return answerService.getAnswersByQuestionTid(questionTid);
    }

    @GetMapping("/{questionTid}")
    public Optional<Question> getQuestionById(@PathVariable String questionTid) {
        return questionService.getQuestionByTid(questionTid);
    }
}

