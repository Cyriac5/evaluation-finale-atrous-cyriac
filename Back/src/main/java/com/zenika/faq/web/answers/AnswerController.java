package com.zenika.faq.web.answers;

import com.zenika.faq.application.AnswerService;
import com.zenika.faq.application.QuestionService;
import com.zenika.faq.domain.model.answers.Answer;
import com.zenika.faq.domain.model.answers.BadLengthAnswerContentException;
import com.zenika.faq.domain.model.questions.BadLengthQuestionTitleException;
import com.zenika.faq.domain.model.questions.Question;
import com.zenika.faq.web.questions.CreateQuestionRequestDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/questions")
public class AnswerController {

    private final AnswerService answerService;

    public AnswerController(AnswerService answerService) {
        this.answerService = answerService;
    }




}
