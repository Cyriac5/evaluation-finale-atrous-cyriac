package com.zenika.faq.web.users;

public class CreatedUserDto {

    private String tid;
    private String username;

    public CreatedUserDto(String tid, String username) {
        this.tid = tid;
        this.username = username;
    }

    public String getTid() {
        return tid;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
