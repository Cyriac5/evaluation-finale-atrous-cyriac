CREATE TABLE IF NOT EXISTS users(
    tid char(36) PRIMARY KEY,
    username text NOT NULL UNIQUE,
    password text NOT NULL
);

CREATE TABLE IF NOT EXISTS questions(
    tid char(36) PRIMARY KEY,
    title text NOT NULL UNIQUE,
    qcontent text NOT NULL,
    postdate TIMESTAMP NOT NULL,
    user_tid char(36) REFERENCES users(tid)
);

CREATE TABLE IF NOT EXISTS answers(
    tid char(36) PRIMARY KEY,
    acontent text NOT NULL,
    answerdate TIMESTAMP NOT NULL,
    question_tid char(36) REFERENCES questions(tid) NOT NULL,
    user_tid char(36) REFERENCES users(tid)

);



