FROM maven:3.8.1-openjdk-17 AS builder
WORKDIR /app
COPY . .
RUN mvn -e -B dependency:resolve
COPY . .
FROM openjdk:slim
WORKDIR /app
COPY --from=builder /app/target/evaluation-finale-atrous-cyriac-1.0-SNAPSHOT.jar .
ENTRYPOINT ["java", "-jar", "./evaluation-finale-atrous-cyriac-1.0-SNAPSHOT.jar"]